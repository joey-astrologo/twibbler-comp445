import java.io.*;
import java.net.*;
import java.util.*;
import java.sql.SQLException;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class TCPServer implements Runnable
{
	Socket socket; // Client Socket
	ServerSocket server = null;
	ServerLog log = null;
	public static ServerLog mainThread = new ServerLog("Main Thread");
	private static List<String> sessionList = Collections.synchronizedList(new ArrayList<String>());
	TCPServer(Socket curSocket, ServerSocket server) {
		this.socket = curSocket;
		this.server = server;
	}
	private void login(String[] requestParam, PrintWriter output) {
		if(requestParam[1]==null || requestParam[1].isEmpty())
		{
			output.println("NACK:LOGIN:" + "No alias was provided.");
			syncLog("Client failed login, no alias provided.");
			return;
		}
		TwibblerDb db = new TwibblerDb();
		String uName = requestParam[1];
		if(!sessionList.contains(uName)) {
			try {
				db.login(uName);
				synchronized(sessionList) {
					sessionList.add(requestParam[1]);
				}
				int index = sessionList.indexOf(requestParam[1]);
				syncLog("Client logged in as " + uName + " with session id:" + index);
				output.println("ACK:LOGIN:" + index);
			}
			catch(Exception e) {
				syncLog("LOGIN failed, alias does not exist in database.");
				output.println("NACK:LOGIN:Alias does not exist.");
			}
		}
		else {
			syncLog("LOGIN failed, someone is already logged in with the requested alias.");
			output.println("NACK:LOGIN:Already logged in.");
		}
	}
	private void register(String[] requestParam, PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		String email = requestParam[1];
		String uName = requestParam[2];
		try {
			if(!uName.toLowerCase().equals("allprofiles") && db.register(email, uName)) {
				output.println("ACK:REG:" + uName);
				syncLog("Client registered new alias "+ uName);
			}
			else {
				output.println("NACK:REG");
				syncLog("Registration failed for "+ uName);
			}

		}
		catch(MySQLIntegrityConstraintViolationException e) {
			syncLog("Registration failed for "+ uName + ", alias already registered.");
			output.println("NACK:REG:User already exists in database.");
		}
		catch(Exception e) {
			syncLog("Registration failed for because of "+ e);
			output.println("NACK:REG:Unknown error, registration aborted.");
		}
	}
	private void profile(String[] requestParam, PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		String uName = requestParam[1];
		String location = requestParam[2];
		String interests = requestParam[3];
		try {
			if(db.createProfile(uName, location, interests)) {
				output.println("ACK:PROFILE:" + uName);
				syncLog("Profile created for "+ uName);
			}
			else {
				output.println("NACK:PROFILE");
				syncLog("Profile creation failed for "+ uName);
			}
		}
		catch(MySQLIntegrityConstraintViolationException e) {
			syncLog("Profile couldn't be created because alias doesn't exist for "+ uName);
			output.println("NACK:PROFILE:Profile couldn't be created because alias doesn't exist.");
		}
		catch(Exception e) {
			syncLog("PROFILE:Unknown error, profile aborted for "+ uName);
			output.println("NACK:PROFILE:Unknown error, profile aborted.");
		}
	}
	private void subscribe(String[] requestParam, PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		int sessionId = Integer.parseInt(requestParam[1]);
		String uName = sessionList.get(sessionId);
		String sAlias = requestParam[2];
		try {
			if(db.subscribe(uName, sAlias)) {
				output.println("ACK:SUB:" + sAlias);
				syncLog("User " + uName + " subscribed to " + sAlias);
			}
			else {
				output.println("NACK:SUB");
				syncLog("User " + uName + " failed to subscribe to " + sAlias);
			}
		}
		catch(Exception e) {
			syncLog("User " + uName + " failed to subscribe to " + sAlias + "; alias is not registered.");
			output.println("NACK:SUB:Alias not in database.");
		}
	}
	private void unsubscribe(String[] requestParam, PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		int sessionId = Integer.parseInt(requestParam[1]);
		String uName = sessionList.get(sessionId);
		String sAlias = requestParam[2];
		try {
			if(db.unsubscribe(uName, sAlias)) {
				output.println("ACK:UNSUB:" + sAlias);
				syncLog("User " + uName + " unsubscribed to " + sAlias);
			}
			else {
				output.println("NACK:UNSUB:You are not Subbed to that alias");
				syncLog("User " + uName + " already to unsubscribe " + sAlias );
			}
		}
		catch(Exception e) {
			syncLog("User " + uName + " failed to unsubscribed to " + sAlias + ", alias is not registered.");
			output.println("NACK:UNSUB:Alias not in database.");
		}
	}
	private void logout(String[] requestParam, PrintWriter output) {
		int sessionId = Integer.parseInt(requestParam[1]);
		String uName = sessionList.get(sessionId);
		synchronized(sessionList) {
			sessionList.remove(sessionId);
		}
		syncLog("User " + uName + " logged out of server.");
		output.println("ACK:LOGOUT:" + sessionId);
	}
	private void unregister(String[] requestParam, PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		int sessionId = Integer.parseInt(requestParam[1]);
		String uName = sessionList.get(sessionId);
		try {
			if(db.unregister(uName)) {
				output.println("ACK:UNREG:" + uName);
				synchronized(sessionList) {
					sessionList.remove(sessionId);
				}
				syncLog("Client " + uName + " unregistered from database.");
			}
			else {
				output.println("NACK:UNREG");
				syncLog("Client " + uName + " failed to unregistered from database.");
			}
		}
		catch(Exception e) {
			syncLog("Client " + uName + " unregistered from database; user doesn't exist.");
			output.println("NACK:UNREG:User doesn't exist.");
		}
	}
	private void post(String[] requestParam, PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		int sessionId = Integer.parseInt(requestParam[1]);
		String uName = sessionList.get(sessionId);
		String message = requestParam[2];
		try {
			if(db.post(message, uName)) {
				output.println("ACK:POST:" + message);
				syncLog("Client " + uName + " twibbled \"" + message + "\" and emails sent out to subscribers.");
			}
			else {
				output.println("NACK:POST:Post failed.");
				syncLog("Client " + uName + "'s twibble could not be posted.");
			}
		}
		catch(Exception e) {
			syncLog("Client " + uName + "'s twibble could not be posted because " + e);
			output.println("NACK:POST:Post could not be saved to twibbler network.");
		}
	}
	private void unpost(String[] requestParam, PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		int sessionId = Integer.parseInt(requestParam[1]);
		String uName = sessionList.get(sessionId);
		int pId = Integer.parseInt(requestParam[2]);
		try {
			if(db.unpost(uName, pId)) {
				output.println("ACK:UNPOST:" +pId);
				syncLog("Client " + uName + " twibble: " + pId + " deleted.");
			}
			else {
				output.println("NACK:UNPOST:Delete post failed.");
				syncLog("Client " + uName + "'s twibble could not be deleted.");
			}
		}
		catch(Exception e) {
			syncLog("Client " + uName + "'s twibble not found in database.");
			output.println("NACK:UNPOST:Post could not be deleted.");
		}
	}

	public void syncLog(String msg) {
		synchronized(this) {
			this.log.write(msg);
		}
	}
	public void run() {
		try {
			PrintWriter output;
			BufferedReader in;
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new PrintWriter(socket.getOutputStream(), true);

			log = new ServerLog("Thread #" + Thread.currentThread().getId());

			String line = new String();
			do
			{
				line = in.readLine();
				String[] requestParam = line.split(":");
				String command = requestParam[0];
				syncLog("Client sent request: " + command);
				if(command.contentEquals("LOGIN")){
					login(requestParam, output);
				}
				if(command.contentEquals("REG")){
					register(requestParam, output);
				}
				if(command.contentEquals("UNREG")){
					unregister(requestParam, output);
				}
				if(command.contentEquals("LOGOUT")){
					logout(requestParam, output);
				}
				if(command.contentEquals("POST")){
					post(requestParam, output);
				}
				if(command.contentEquals("UNPOST")){
					unpost(requestParam, output);
				}
				if(command.contentEquals("PROFILE")){
					profile(requestParam, output);
				}
				if(command.contentEquals("SUB")){
					subscribe(requestParam, output);
				}
				if(command.contentEquals("UNSUB")){
					unsubscribe(requestParam, output);
				}
				if (command.equals("WHOIS")) {
					synchronized(sessionList) {
						output.println("ACK:WHOIS:" + sessionList);
					}
				}
				if (command.equals("QUIT")) {
					if(requestParam.length > 1) {
						logout(requestParam, output);
					}
					output.println("ACK:QUIT");
					break;
				}
				output.flush();
			}while(true);

			// Close all the input and output streams, as well as the socket
			syncLog("Thread closing...");
			in.close();
			output.close();
			socket.close();
			syncLog("Thread closed.");

		} catch (Exception e) {
			syncLog("Client unexpectedly disconnected.");
		}
	}

	public static void main(String[] args) 
	{
		ServerSocket server = null;
		int serverPort = 6789;
		try {
			server = new ServerSocket(serverPort);
		}catch(Exception e) {

		}
		new Thread(new HTTPServer()).start();
		mainThread.write("Server is listening for requests.");
		while(true) {
			try{
				Socket sock = server.accept();
				new Thread(new TCPServer(sock, server)).start();
				mainThread.write("Client connected.");
			}catch(Exception e) {
				mainThread.write("Server quit: " + e);
			}
		} 

	}

}
