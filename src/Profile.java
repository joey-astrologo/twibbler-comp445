import java.sql.Date;

public class Profile {
	private String username = null;
	private String email = null;
	private String location = null;
	private String[] interest = null;
	private String timestamp = null;

	public Profile(String username, String email, String location,
			String interests, String registered) {
		setUsername(username);
		setEmail(email);
		setLocation(location);
		setInterest(interests);
		setTimestamp(registered);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		if(location!=null)
			this.location = location;
		else
			this.location = "";
	}

	public String[] getInterest() {
		return interest;
	}

	/**
	 * Split with default comma delimited
	 * 
	 * @param interest
	 *            - Comma delimited string
	 */
	public void setInterest(String interest) {
		if(interest!=null)
			this.interest = interest.split(",");
		else
			this.interest = new String[0];
	}

	public void setInterest(String interest, String split) {
		if(interest!=null)
			this.interest = interest.split(split);
		else
			this.interest = new String[0];
	}

	public void setInterest(String[] interest) {
		if(interest!=null)
			this.interest = interest;
		else
			this.interest = new String[0];
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
	}
}
