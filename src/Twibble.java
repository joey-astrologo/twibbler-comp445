/**
 * 
 */

/**
 * Class definition of a twibble.
 * @author Daniel
 */
public class Twibble {
	private String message = "";
	private int id = -1;
	
	public Twibble(int id, String msg){
		this.id = id;
		this.message = msg;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
