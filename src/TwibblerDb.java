import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class TwibblerDb {
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	private Connection connectDB() throws SQLException, ClassNotFoundException{
		/*
		// This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
	          .getConnection("jdbc:mysql://localhost/twibbler?"
	              + "user=sqluser&password=sqluserpw");
	      */
		    Connection c = null;
		    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:twibbler.db");
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
	      return c;
	 }
	 public int login(String uName) throws SQLException, ClassNotFoundException{
		  connect = connectDB();
	      preparedStatement = connect
		          .prepareStatement("SELECT id FROM users WHERE name = ?; ");
	      
	      preparedStatement.setString(1, uName);
	      
		  resultSet = preparedStatement.executeQuery();
		  resultSet.next();
		  int id = resultSet.getInt("id");
		  close();
		  return id;
	  }
	 public ArrayList<String> getUsers() throws SQLException, ClassNotFoundException{
		  ArrayList<String> aliases = new ArrayList<String>();
		  connect = connectDB();
	      preparedStatement = connect
		          .prepareStatement("SELECT name FROM users; ");
	      
		  resultSet = preparedStatement.executeQuery();
		  while(resultSet.next())
		  {
			  aliases.add(resultSet.getString("name"));
		  }
		  close();
		  return aliases;
	  }
	 public ArrayList<Twibble> getPosts(String uName) throws SQLException, ClassNotFoundException{
		 int id = login(uName);
		 ArrayList<Twibble> postIds = new ArrayList<Twibble>();
		  connect = connectDB();
	      
	      preparedStatement = connect
		          .prepareStatement("SELECT p.id, p.message FROM posts p JOIN postsOf po ON po.post_id = p.id where po.user_id = ? ORDER BY p.id desc;");
	      preparedStatement.setInt(1, id);
		  resultSet = preparedStatement.executeQuery();
		  while(resultSet.next())
		  {
			  postIds.add(new Twibble(resultSet.getInt("id"),resultSet.getString("message")));
		  }
		  close();
		  return postIds;
	  }
	 public Profile getUserProfile(String uName) throws ClassNotFoundException, SQLException{
		 int id = login(uName);
		 connect = connectDB();
		 PreparedStatement preparedStatement = connect.prepareStatement(
				"SELECT name, email, date_time "
		 		+ "FROM users WHERE id = ?;");
		 preparedStatement.setInt(1, id);
		  resultSet = preparedStatement.executeQuery();
		  
		  PreparedStatement preparedStatement2 = connect.prepareStatement(
					"SELECT location, interests "
			 		+ "FROM profiles "
			 		+ "INNER JOIN profileOf ON profiles.id = profileOf.profile_id "
			 		+ "WHERE profileOf.user_id = ?;");
			 preparedStatement2.setInt(1, id);
			 ResultSet resultSet2 = preparedStatement2.executeQuery();
		  if(resultSet.next()){
			  String name = resultSet.getString("name");
			  String email = resultSet.getString("email");
			  String date = resultSet.getString("date_time");
			  resultSet2.next();
			  String location = resultSet2.getString("location");
			  String interests = resultSet2.getString("interests");
			  close();
			  return new Profile(name,email,location,interests, date);
		  }
		  else{
			  close();
			  return null;
		  }
		  
	 }
	 public boolean post(String message, String uName) throws Exception {
		  int id = login(uName);
		  connect = connectDB();
	      preparedStatement = connect
	          .prepareStatement("INSERT INTO  posts(message, date_time) VALUES (?, datetime('now', 'localtime')); ", 
	        		  Statement.RETURN_GENERATED_KEYS);
	      preparedStatement.setString(1, message);
	      if(preparedStatement.executeUpdate() == 0) {
	    	  return false;
	      }
	      int postId = -1;
	      ResultSet rs = preparedStatement.getGeneratedKeys();
	      if (rs.next()){
	          postId=rs.getInt(1);
	      }
	      if(postId != -1) {
		      	preparedStatement = connect
			          .prepareStatement("INSERT INTO  postsOf VALUES (?, ?); ");
			      
			      preparedStatement.setInt(1, id);
			      preparedStatement.setInt(2, postId);
			      if(preparedStatement.executeUpdate() == 0) {
			    	  return false;
			      }
			      
			      preparedStatement = connect
				          .prepareStatement("SELECT email FROM users JOIN subscription "
				          		+ "ON users.id = subscription.user_id "
				          		+ "WHERE subscription.sub_id= ?; ");
			      preparedStatement.setInt(1, id);
			      resultSet = preparedStatement.executeQuery();
			      while(resultSet.next())
				  {
					  //emails.add(resultSet.getString("email"));
			    	  Mail.sendMail(resultSet.getString("email"), uName, message);
				  }
	      }
	      else {
	    	  return false;
	      }
		  close();
		  return true;
	 }
	 public boolean unpost(String uName, int pId) throws Exception {
		  int uId = login(uName);
		  connect = connectDB();
	      preparedStatement = connect
	          .prepareStatement("DELETE FROM postsOf "
	          		+ "WHERE post_id= ? AND user_id= ?;");
	      preparedStatement.setInt(1, pId);
	      preparedStatement.setInt(2, uId);
	      if(preparedStatement.executeUpdate() == 0) {
	    	  return false;
	      }
	      
	      preparedStatement = connect
		          .prepareStatement("DELETE FROM posts "
		          		+ "WHERE id= ? ;");
		      preparedStatement.setInt(1, pId);
		      
		      if(preparedStatement.executeUpdate() == 0) {
		    	  return false;
		      }
		  close();
		  return true;
	 }
	 public boolean unregister(String uName) throws SQLException, ClassNotFoundException{
		  int id = login(uName);
		 connect = connectDB();
	      preparedStatement = connect
		          .prepareStatement("DELETE FROM users WHERE id = ?; ");
	      
	      preparedStatement.setInt(1, id);
	      
	      preparedStatement.executeUpdate();
	    
	      preparedStatement = connect
		          .prepareStatement("DELETE FROM posts WHERE id IN (SELECT post_id FROM postsOf WHERE user_id = ?); ");
	      
	      preparedStatement.setInt(1, id);
	      
	      preparedStatement.executeUpdate();
	      
	      preparedStatement = connect
		          .prepareStatement("DELETE FROM postsOf WHERE user_id = ?; ");
	      
	      preparedStatement.setInt(1, id);
	      
	      preparedStatement.executeUpdate();
	      
	      preparedStatement = connect
		          .prepareStatement("DELETE FROM profiles WHERE id IN (SELECT profile_id FROM profileOf WHERE user_id = ?); ");
	      
	      preparedStatement.setInt(1, id);
	      
	      preparedStatement.executeUpdate();
	      
	      preparedStatement = connect
		          .prepareStatement("DELETE FROM profileOf WHERE user_id = ?; ");
	      
	      preparedStatement.setInt(1, id);
	      
	      preparedStatement.executeUpdate();
	      preparedStatement = connect
		          .prepareStatement("DELETE FROM subscription WHERE user_id = ? OR sub_id = ?; ");
	      
	      preparedStatement.setInt(1, id);
	      preparedStatement.setInt(2, id);
	      
	      preparedStatement.executeUpdate();
	      
		  close();
		  return true;
	  }
	 
	  public boolean register(String email, String uName) throws SQLException, ClassNotFoundException{
		  connect = connectDB();
	      preparedStatement = connect
	          .prepareStatement("INSERT INTO  users(name, email, date_time) VALUES (?, ?, datetime('now', 'localtime')); ");
	      preparedStatement.setString(1, uName);
	      preparedStatement.setString(2, email);
	      if(preparedStatement.executeUpdate() == 0) {
	    	  return false;
	      }
		  close();
		  return true;
	  }
	  public boolean createProfile(String uName, String location, String interests) throws SQLException, ClassNotFoundException{
		  int uId = login(uName);
		  connect = connectDB();
	      preparedStatement = connect
	          .prepareStatement("INSERT INTO  profiles(location, interests) VALUES (?, ?); ",
	        		  Statement.RETURN_GENERATED_KEYS);
	      preparedStatement.setString(1, location);
	      preparedStatement.setString(2, interests);
	      if(preparedStatement.executeUpdate() == 0) {
	    	  return false;
	      }
	      int profileId = -1;
	      ResultSet rs = preparedStatement.getGeneratedKeys();
	      if (rs.next()){
	          profileId=rs.getInt(1);
	      }
	      if(profileId != -1) {
	    	  preparedStatement = connect
	    	          .prepareStatement("INSERT INTO  profileOf VALUES (?, ?); ");
	    	  
	    	  preparedStatement.setInt(1, uId);
		      preparedStatement.setInt(2, profileId);
		      if(preparedStatement.executeUpdate() == 0) {
		    	  return false;
		      }
	      }
		  close();
		  return true;
	  }
	  public boolean subscribe(String uName, String sAlias) throws SQLException, ClassNotFoundException{
		  int uId = login(uName);
		  int sId = login(sAlias);
		  connect = connectDB();
	      preparedStatement = connect
	          .prepareStatement("INSERT INTO subscription VALUES (?, ?); ");
	      preparedStatement.setInt(1, uId);
	      preparedStatement.setInt(2, sId);
	      if(preparedStatement.executeUpdate() == 0) {
	    	  return false;
	      }
		  close();
		  return true;
	  }
	  public boolean unsubscribe(String uName, String sAlias) throws SQLException, ClassNotFoundException{
		  int uId = login(uName);
		  int sId = login(sAlias);
		  connect = connectDB();
	      preparedStatement = connect
	          .prepareStatement("DELETE FROM subscription WHERE user_id= ? AND sub_id= ?; ");
	      preparedStatement.setInt(1, uId);
	      preparedStatement.setInt(2, sId);
	      if(preparedStatement.executeUpdate() == 0) {
	    	  return false;
	      }
		  close();
		  return true;
	  }

	  // You need to close the resultSet
	  private void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
}
