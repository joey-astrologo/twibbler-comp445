import java.net.*;
import java.util.regex.Pattern;
import java.io.*;

public class TCPClient 
{
	public static int userId = -1;
	public static boolean badFlag = false;
	public static PrintWriter output;
	public static BufferedReader in;
	public static BufferedReader userInput;
	private static final Pattern rfc2822 = Pattern.compile(
	        "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
	);
	private static void start(String option, BufferedReader bf, PrintWriter output) throws IOException
	{
        String username;
        String email;
        String location;
        String interests;
        
        switch(option.toLowerCase()){
        	case "login":    username = promptInput("What is your alias?",bf);
            			  	 option = "LOGIN:" + username;
            			  	 output.println(option);
                          	 break;
            case "register": username = promptInput("What would you like your alias to be?",bf);
						  	 do {
						  		 email = promptInput("And your email?",bf);
						  		 if(!rfc2822.matcher(email).matches()) {
						  			 System.out.println("Invalid email, please try again.");
						  		 }
						  	 }while(!rfc2822.matcher(email).matches()); 
						     option = "REG:" + email + ":" + username;
						     output.println(option);
						     //readFromServer(in);
						  	 location = promptInput("What is your location?",bf);
						  	 interests = promptInput("What are your interests? (Seperate them with commas)",bf);
						     option = "PROFILE:" + username + ":" + location + ":" + interests;
						     output.println(option);
			                 break;
            case "whois":    System.out.println("Checking who is logged into server");
				 			 output.println("WHOIS");
				 			 break;
            case "quit": 	 option = "QUIT";
            				 output.println(option);
            				 System.out.println("Goodbye");
            		      	 break;
            default: badFlag = true;
        }
	}
	private static void system(String option, BufferedReader bf, PrintWriter output) throws IOException{
		String message = "";
		switch(option){
	        case "logout": 
	        				option = "LOGOUT:" + userId;
	        				output.println(option);
	                        break;
	        case "unregister":  
	        				System.out.println("Unregistering current user from Twibbler network");
				 			output.println("UNREG:" + userId);
				 			break;
	        case "whois":    
	        				System.out.println("Checking who is logged into server");
		  					output.println("WHOIS");
		  					break;
	        case "post":    
	        				message = promptInput("What's your twibble?",bf);
				 			output.println("POST:" + userId + ":" + message);
				 			break;
	        case "unpost":  int pId = Integer.parseInt(promptInput("What's the id number of the post you want to delete?",bf));
							output.println("UNPOST:" + userId + ":" + pId);
							break;
	        case "quit":    option = "QUIT:" + userId;;
			                output.println(option);
	        		        System.out.println("Goodbye");
	        		        break;
	        case "subscribe": 	message = promptInput("Who do you want to subscribe to (input their alias)?",bf);
								output.println("SUB:" + userId + ":" + message);
								break;
	        case "unsubscribe": 	message = promptInput("Who do you want to unsubscribe to (input their alias)?",bf);
									output.println("UNSUB:" + userId + ":" + message);
									break;
	        default: badFlag = true;
		}
	}
	private static void serverCommands(String[] requestParam, BufferedReader in) throws IOException {
		String command = requestParam[0];
		
		if(command.contentEquals("ACK"))
		{
			command = requestParam[1];
			if(command.contentEquals("LOGIN"))
			{
				userId = Integer.parseInt(requestParam[2]);
			}
			if(command.contentEquals("LOGOUT"))
			{
				userId = -1;
			}
			if(command.contentEquals("REG"))
			{
				readFromServer(in);
			}
			if(command.contentEquals("WHOIS"))
			{
				System.out.println(requestParam[2]);
			}
			if(command.contentEquals("UNREG"))
			{
				System.out.println(requestParam[2] + " has been unregistered from the twibbler network"
						+ " and logged out of this client.");
				userId = -1;
			}
		}
		else 
		{
			System.out.println(requestParam[1] + " command failed.");
			if(requestParam.length >= 3) {
				System.out.println(requestParam[2]);
			}
		}
	}
	
	private static String promptInput(String message, BufferedReader br) throws IOException
	{
		String input = "";
		while(input.isEmpty())
		{
			System.out.println(message);
			System.out.print(">");
			input = br.readLine();
			if(input.trim().isEmpty())
			{
				input = "";
				System.out.println("No input was provided.");
			}
		}
		return input.trim();
	}
	public static void readFromServer(BufferedReader in) throws IOException {
		String mLine = in.readLine();
		
		//System.out.println("FROM SERVER: " + mLine);
		String[] requestParam = mLine.split(":");
		serverCommands(requestParam, in);
	}
	public static void main(String[] args)
	{
		Socket socket = null;
		
		try {
			int serverPort = 6789;
			InetAddress host = InetAddress.getByName("localhost");
			//InetAddress host = InetAddress.getByName("192.168.1.102");
			//InetAddress host = InetAddress.getByName("173.177.160.50");
			socket = new Socket(host, serverPort);
		
			output = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			// Get input from the user
			userInput = new BufferedReader(new InputStreamReader(System.in));
			String line;
			
			while(true)
			{	
				if(userId == -1) {
					System.out.println("Options: login, register.");
					System.out.print(">");
					line = userInput.readLine();
					start(line, userInput, output);
				}
				else
				{
					System.out.println("Options: post, unpost, subscribe, unsubscribe, logout, unregister");
					System.out.print(">");
					line = userInput.readLine();
					system(line, userInput, output);
				}
				
				System.out.println("Sent \"" + line + "\" to the server");
				if(badFlag) {
					System.out.println("Invalid command not sent to server.");
					badFlag = false;
				}
				else {
					readFromServer(in);
				}
				if(line.contentEquals("quit")) 
					break;
			}
			
			// Close all the input and output streams, as well as the socket
			in.close();
			output.close();
			userInput.close();
			socket.close();
		} catch (UnknownHostException e) {
			System.out.println("UnknownHostException:" + e.getMessage());
		} catch (IOException e) {
			System.out.println("IOException:" + e.getMessage());
		}
	}
}
