import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class HTTPServer implements Runnable {
	private ServerSocket httpServer = null;
	ServerLog log = null;
	private boolean enabled = true;

	public HTTPServer() {
		try {
			httpServer = new ServerSocket(6788);
		} catch (IOException e) {
			syncLog("IOException thrown when initializing http server.");
		}
	}

	/**
	 * Starts listening to incoming requests while enabled.
	 */
	private void start() {
		while (enabled) {
			try {
				Socket socket = httpServer.accept();
				parseRequest(socket);
			} catch (IOException e) {
				syncLog("IOException thrown when accepting an http connection.");
			}
		}

	}

	private void parseRequest(Socket socket) {
		//TODO LOG
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			ArrayList<String> request = new ArrayList<String>();
			String line = null;
			while ((line = in.readLine()) != null && !line.trim().isEmpty()) {
				request.add(line);
			}
			if (request.contains("GET /allprofiles HTTP/1.1")
					|| request.contains("GET /allprofiles/ HTTP/1.1")) {
				getall(output);
			}
			else
			{
			if (request.size() > 0
					&& request.get(0).matches(
							"GET /([a-zA-Z0-9.%_-]+)/? HTTP/1.1")) {
				get(URLDecoder.decode(request.get(0), "UTF-8"), output);
			}
			}
			output.flush();
			output.close();
			in.close();
		} catch (IOException e) {
			syncLog("IOException thrown when parsing the request.");
		}
	}

	private void getall(PrintWriter output) {
		TwibblerDb db = new TwibblerDb();
		ArrayList<String> users = null;
		try {
			users = db.getUsers();
		} catch (Exception e) {
			syncLog("Error occured while getting the list of users.");
			output.println("<html><head><title>Twibbler</title></head><body><h1>List of registered users:</h1><p>Error occured while getting the list of users.</p></body></html>");
			users = null;
		}
		if (users != null) {
			String htmlList = "";
			if (users.isEmpty()) {
				htmlList = "<p>No users are currently registered</p>";
			} else {
				htmlList = "<ul>";
				for (String user : users) {
					htmlList += "<li><a href='/" + user + "'>" + user
							+ "</a></li>";
				}
				htmlList += "</ul>";
			}
			File profilePage = new File("src/allprofiles.txt");
			String dynamicHtml = "";
			if(profilePage.exists()){
				try {
					List<String> content = Files.readAllLines(profilePage.toPath(), Charset.forName("UTF-8"));
					for(String str:content){
						dynamicHtml+=str;
					}
					dynamicHtml = dynamicHtml.replace("<htmlList>", htmlList);
					output.print("HTTP/1.1 200 OK\r\n Content-Type: text/html\r\nContent-Length: "
							+ dynamicHtml.length() + "\r\n\r\n" + dynamicHtml);
				} catch (IOException e) {
					syncLog("Error occured while reading allprofiles.txt");
					output.println("<html><head><title>Twibbler</title></head><body><h1>List of registered users:</h1><p>Error occured while getting the list of users.</p></body></html>");
				}
			}
		}
	}

	private void get(String request, PrintWriter output) {
		String profileName = request.substring(5, request.length() - 9);
		TwibblerDb db = new TwibblerDb();
		Profile profile = null;
		ArrayList<Twibble> twibbles = null;
		try {
			profile = db.getUserProfile(profileName);
			twibbles = db.getPosts(profileName);
		} catch (Exception e) {
			syncLog("Error occured while getting the user profiles or posts of: "+profileName);
		}
		if(profile!=null)
		{
			String username = "Profile: "+profile.getUsername();
			String dateJoined = "Member since: "+profile.getTimestamp();
			String location = "Location: "+profile.getLocation();
			String interests = "Interests: <ul>";
			for(String interest:profile.getInterest())
			{
				interests+="<li>"+interest+"</li>";
			}
			interests+="</ul>";
			String htmlPosts = "";
			if(twibbles!=null && !twibbles.isEmpty()){
				htmlPosts = "Posts: <table><tr><th>Id</th><th>Twibble</th></tr>";
				for(Twibble t:twibbles)
				{
					htmlPosts+="<tr><td>"+t.getId()+"</td><td>"+t.getMessage()+"</td></tr>";
				}
				htmlPosts+="</table>";

			}
			else
			{
				htmlPosts = "This user did not twibble yet.";
			}
			File profilePage = new File("src/profile.txt");
			String dynamicHtml = "";
			if(profilePage.exists()){
				try {
					List<String> content = Files.readAllLines(profilePage.toPath(), Charset.forName("UTF-8"));
					for(String str:content){
						dynamicHtml+=str;
					}
					dynamicHtml = dynamicHtml.replace("<username>", username);
					dynamicHtml = dynamicHtml.replace("<dateJoined>", dateJoined);
					dynamicHtml = dynamicHtml.replace("<location>", location);
					dynamicHtml = dynamicHtml.replace("<interests>", interests);
					dynamicHtml = dynamicHtml.replace("<htmlPosts>", htmlPosts);
					output.print("HTTP/1.1 200 OK\r\n Content-Type: text/html\r\nContent-Length: "
							+ dynamicHtml.length() + "\r\n\r\n" + dynamicHtml);
				} catch (IOException e) {
					syncLog("Error occured while reading profile.txt");
					profileFail(output);
				}
			}
			else{
				syncLog("ERROR - Profile.txt could not be found!");
				profileFail(output);
			}
			
		}
		else{
			syncLog("Error occured while reading profile.txt");
			profileFail(output);
		}

	}
	
	private void profileFail(PrintWriter output){
		String failHtml = "<html><head><title>Twibbler</title></head><body><h1>Profile: </h1><p>Error occured when trying to obtain the profile and twibbles.<br><a href='/allprofiles'>Return to profile list</a></p></body></html>";
		output.print("HTTP/1.1 200 OK\r\n Content-Type: text/html\r\nContent-Length: "
				+ failHtml.length() + "\r\n\r\n" + failHtml);
	}

	public void setEnabled(boolean b) {
		this.enabled = b;
	}
	
	public void syncLog(String msg) {
		synchronized(this) {
			this.log.write(msg);
		}
	}

	@Override
	public void run() {
		log = new ServerLog("HttpServerLog");
		syncLog("Http Server Started");
		start();
	}

}
