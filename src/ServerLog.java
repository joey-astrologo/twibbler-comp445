import java.io.IOException;
import java.net.URL;
import java.util.logging.*;

public class ServerLog {
	Logger logger = null;
	public ServerLog(String name) {
		logger = Logger.getLogger(name);  
	    FileHandler fh; 
	    try {  
	    	//current working directory
			URL location = ServerLog.class.getProtectionDomain().getCodeSource().getLocation();
	        String fileName = location.getFile() + name + ".txt";
	        if(fileName.startsWith("/"))
	        	fileName = fileName.substring(1);
	        fh = new FileHandler(fileName);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  
	}
	public void write(String msg) {
		logger.info(msg);
	}
		
}
